'use strict';

angular.module('SimpleAssertion', [])

.factory('Assert', function() {
	var self = {
		isNumberGreaterThan : function(value, base) {
			/*
			 * Asserts that value is greater than base
			 *
			 * @param {number} value: The value to be compared
			 * @param {number} base: The value to compare against
			 */
			this.isNumber(value);
			this.isNumber(base);
			
			if (value <= base) {
				throwError(value + ' > ' + base, '');
			}
		},
		
		isNumberGreaterThanOrEqualTo : function(value, base) {
			/*
			 * Asserts that value is greater than or equal to base
			 *
			 * @param {number} value: The value to be compared
			 * @param {number} base: The value to compare against
			 */
			this.isNumber(value);
			this.isNumber(base);
			
			if (value < base) {
				throwError(value + ' >= ' + base, '');
			}
		},
		
		isBoolean : function(value){
			if(typeof(value) !== 'boolean') {
				throwError(getTypeof(value), 'boolean');
				return false;
			}
			return true;
		},

		isBooleanOrNull : function(value) {
			if(value === null) { return true; }
			return this.isBoolean(value);
		},		

		isDate : function(value){
			if(!angular.isDate(value)) {
				throwError(getTypeof(value), 'date');
				return false;
			}
			return true;
		},	

		isDateOrNull : function(value) {   
			if(value === null) { return true; }
			return this.isDate(value);
		},			

		isNumber : function(value) {
			if(!angular.isNumber(value)) {
				throwError(getTypeof(value), 'number');
				return false;
			}
			if(isNaN(value)) {
				throwError('NaN', 'number');
				return false;
			}		
			return true;
		},

		isNumberOrNull : function(value) {
			if(value === null) { return true; }
			return this.isNumber(value);
		},

		isString : function(value) {
			if(!angular.isString(value)) {
				throwError(getTypeof(value), 'string');
				return false;
			}
			return true;			
		},	

		isStringOrNull : function(value) {
			if(value === null) { return true; }
			return this.isString(value);
		},			

		isStringAndNotEmpty : function(value) {
			if(!this.isString(value)) { return false; }
			if(value.length <= 0) {
				throwError('empty string', 'string with stuff in it' );
				return false;
			}	
		},

		isFunction : function(value) {
			if(!angular.isFunction(value)) {
				throwError(getTypeof(value), 'function');
				return false;
			}	
			return true;			
		},		

		isArray : function(value) {
			if(!angular.isArray(value)) {
				throwError(getTypeof(value), 'array');
				return false;
			}	
			return true;			
		},			

		isArrayNotEmpty : function(value) {
			if(!this.isArray(value)) { return false; }
			if(value.length <= 0) {
				throwError('empty array', 'array with stuff in it' );
				return false;
			}			
			return true;			
		},		

		isArrayLengthEqual : function(value, length) {
			if(!this.isArray(value)) { return false; }
			if(!this.isNumber(length)) { return false; }
			if(value.length !== length) {
				throwError('array length', 'array invalid length' );
				return false;
			}			
			return true;			
		},	

		isObject : function(value) {
			if(!angular.isObject(value)) {
				throwError(getTypeof(value), 'object');
				return false;
			}
			return true;			
		},

		isObjectOrNull : function(value) {
			if(value === null) { return true; }
			return this.isObject(value);		
		},		

		isObjectEqual : function(value, model) {
			if(!this.isObject(value)) { return false; }
			if(!this.isObject(model)) { return false; }

			for(var i in model) {
				if(!this.isAssertableType( model[i] )) { return false; }
				
				var gotType = getTypeof( value[i] );
				var expectedType = getTypeof( model[i]() );

				if(gotType !== expectedType) {
					throwError(gotType, expectedType + ' of type \'' + i + '\'');
					return false;
				}
			}

			return true;
		},

		isAngularRequestObject : function(value) {
			//This is an angular request object
			var model = {
				config			: Object,
				data			: Object,
				status			: Number
			};

			if(!this.isObject(value)) { return false; }
			if(!this.isObjectEqual(value, model)) { return false; }
		},		

		isInstanceOf : function(value, expectedType) {
			if(!this.isAssertableType(expectedType)) { return false; }

			if(getTypeof(value) !== getTypeof( expectedType() )) {
				throwError(getTypeof(value), getTypeof( expectedType() ));
			}
			return true;
		},

		isAssertableType : function(value) {
			if(!self.isFunction(value)) { return false; }

			value = value();
			if(!validateType( getTypeof(value) )) {
				throwError(value, 'boolean, number, string, function, object or date');
				return false;
			}			
			return true;	
		}
	};

	/* Private Methods */
	var throwError = function(gotType, expectedType) {
		var message = '';
		message += 'Assertion Fail: Expected ' + expectedType + ' got: \'' + gotType + '\'\n';
		message += getStackTrace();
		
		if(getTypeof(message) === 'string') {
			if(console) { console.trace(); }
			throw message;
		} else {
			if(console) { console.trace(); }
			throw 'Unknown error occured';			
		}
	};

	//We have to wrap typeof becuase typeof(null) returns 'object'...FU javascript
	var getTypeof = function(value) {
		if(value === null) {
			return 'null';
		} else {
			return typeof(value);
		}
	};

	//Expects String value of the type
	var validateType = function(type) {
		if(typeof(type) !== 'string') { return false; }
		type = type.toLowerCase();
		if(type === 'boolean') {
			return true;
		} else if(type === 'number') {
			return true;
		} else if(type === 'string') {
			return true;
		} else if(type === 'object') {
			return true;
		} else if(type === 'function') {
			return true;
		} 	
		return false;
	};

	var getStackTrace = function() {
		var stackTrace = '';
		try {
			var obj = {};
			Error.captureStackTrace(obj, getStackTrace);
			stackTrace = obj.stack;
		} catch(ex) {}
		return stackTrace;
	};
	return self;
});










