'use strict';

describe('SimpleAssertion', function() {

	beforeEach(module('SimpleAssertion'));


	describe('Assert defintions', function() {
		var Assert = null;
		beforeEach(inject(function(_Assert_) {
			Assert = _Assert_;
		}));

		//TODO: break this down into smaller tests
		it('assert and public/private methods are defined/hidden', function() {
			//Public Methods
			expect(Assert).toBeDefined();
			expect(Assert.isBoolean).toBeDefined();
			expect(Assert.isBooleanOrNull).toBeDefined();
			expect(Assert.isDate).toBeDefined();
			expect(Assert.isDateOrNull).toBeDefined();
			expect(Assert.isNumber).toBeDefined();
			expect(Assert.isNumberOrNull).toBeDefined();
			expect(Assert.isString).toBeDefined();
			expect(Assert.isStringOrNull).toBeDefined();
			expect(Assert.isStringAndNotEmpty).toBeDefined();
			expect(Assert.isFunction).toBeDefined();
			expect(Assert.isArray).toBeDefined();
			expect(Assert.isArrayNotEmpty).toBeDefined();
			expect(Assert.isArrayLengthEqual).toBeDefined();
			expect(Assert.isObject).toBeDefined();
			expect(Assert.isObjectEqual).toBeDefined();
			expect(Assert.isAngularRequestObject).toBeDefined();
			expect(Assert.isInstanceOf).toBeDefined();
			expect(Assert.isAssertableType).toBeDefined();
			expect(Assert.isAssertableType).toBeDefined();


			//Private Methods
			expect(Assert.throwError).not.toBeDefined();
			expect(Assert.abstractThrow).not.toBeDefined();
			expect(Assert.getTypeof).not.toBeDefined();
			expect(Assert.validateType).not.toBeDefined();
			expect(Assert.getStackTrace).not.toBeDefined();
		});

		describe('isBoolean Tests', function() {
			it('"0" should not throw an error', function() {
				expect(function(){ Assert.isNumber(0); }).not.toThrow();
			});	
			it('"1" should not throw an error', function() {
				expect(function(){ Assert.isNumber(1); }).not.toThrow();
			});	
			it('"55" should not throw an error', function() {
				expect(function(){ Assert.isNumber(55); }).not.toThrow();
			});	
			it('"-55" should not throw an error', function() {
				expect(function(){ Assert.isNumber(-55); }).not.toThrow();
			});	
			it('"null" should throw an error', function() {
				expect(function(){ Assert.isNumber(null); }).toThrow();
			});	
			it('"true" should throw an error', function() {
				expect(function(){ Assert.isNumber(true); }).toThrow();
			});				
			it('"false" should throw an error', function() {
				expect(function(){ Assert.isNumber(false); }).toThrow();
			});			
			it('"[]" should throw an error', function() {
				expect(function(){ Assert.isNumber([]); }).toThrow();
			});	
			it('"" should throw an error', function() {
				expect(function(){ Assert.isNumber(""); }).toThrow();
			});	
			it('"Testing 123" should throw an error', function() {
				expect(function(){ Assert.isNumber("Testing 123"); }).toThrow();
			});				
			it('"Object()" should throw an error', function() {
				expect(function(){ Assert.isNumber(Object()); }).toThrow();
			});		
			it('"Function()" should throw an error', function() {
				expect(function(){ Assert.isNumber(Function()); }).toThrow();
			});	
			it('"Array()" should throw an error', function() {
				expect(function(){ Assert.isNumber(Array()); }).toThrow();
			});		
			it('"Number()" should not throw an error', function() {
				expect(function(){ Assert.isNumber(Number()); }).not.toThrow();
			});	
			it('"String()" should throw an error', function() {
				expect(function(){ Assert.isNumber(String()); }).toThrow();
			});	
			it('"Boolean()" should throw an error', function() {
				expect(function(){ Assert.isNumber(Boolean()); }).toThrow();
			});								
		});


		describe('isObjectOrNull Tests', function() {
			it('Null should not throw an error', function() {
				expect(function(){ Assert.isObjectOrNull(null); }).not.toThrow();
			});	
			it('"" should throw an error', function() {
				expect(function(){ Assert.isObjectOrNull(""); }).toThrow();
			});	

		});

	});


});