'use strict';

module.exports = function (grunt) {

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    connect: {
      main: {
        options: {
          port: 9001
        }
      }
    },
    watch: {
      main: {
        options: {
          livereload: true,
          livereloadOnError: false,
          spawn: false
        },
        files: ['angular-simple-assertion.js','dist/**/*'],
        tasks: ['test']
      }
    },
    jshint: {
      main: {
        options: {
          jshintrc: '.jshintrc'
        },
        src: 'angular-simple-assertion.js'
      }
    },
    jasmine: {
      unit: {
        src: ['./bower_components/jquery/dist/jquery.js','./bower_components/angular/angular.js','./bower_components/angular-mocks/angular-mocks.js','./dist/angular-simple-assertion.js'],
        options: {
          specs: 'test/*.js'
        }
      }
    },
    concat: {
      main: {
        src: ['angular-simple-assertion.js'],
        dest: 'dist/angular-simple-assertion.js'
      }
    },
    uglify: {
      main: {
        files: [
        {src:'dist/angular-simple-assertion.js',dest:'dist/angular-simple-assertion.min.js'}
        ]
      }
    }

  });

  grunt.registerTask('serve', ['jshint','connect', 'watch']);
  grunt.registerTask('build', [ 'concat','uglify']);
  grunt.registerTask('test', ['build', 'jshint','jasmine']);

};