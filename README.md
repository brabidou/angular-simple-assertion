angular-simple-assertion [![Build Status](https://travis-ci.org/brabidou/angular-simple-assertion.svg?branch=master)](https://travis-ci.org/brabidou/angular-simple-assertion)
========================

Angular library for doing simple assertions

Installation and usage
--
bower install angular-simple-assertion

<pre><code>
openAbstractModal : function(modalRef, name) {
	Assert.isObject(modalRef);
	Assert.isStringAndNotEmpty(name);
};
</code></pre>

Build and Testing
--
npm install

bower install

grunt test

grunt build



